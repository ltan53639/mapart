const upload = document.getElementById('upload');
const datList = document.getElementById('datlist');
const mapWidth = document.getElementById('mapwidth');
const mapHeight = document.getElementById('mapheight');
const mapCanvas = document.getElementById('map');
const jsonOut = document.getElementById('jsonout');
const exportBtn = document.getElementById('export');
const title = document.getElementById('title');
const authors = document.getElementById('authors');
const scrollToTop = document.getElementById('scrolltotop');
const scrollToBottom = document.getElementById('scrolltobottom');
const settings = document.getElementById('settings');
const options = document.getElementById('options');

const nbt = require('prismarine-nbt');
// I fucking hate having to use Node libraries but I'm too lazy to write my own NBT implementation
const { Buffer } = require('buffer');

const mapCtx = mapCanvas.getContext('2d', { alpha: false });

let mapArrangement;

function scaleMap(){
    mapCanvas.width = mapWidth.value * 128;
    mapCanvas.height = mapHeight.value * 128;
    mapArrangement = new Array(mapWidth.value * mapHeight.value);
}
mapWidth.onchange = scaleMap;
mapHeight.onchange = scaleMap;
scaleMap();

jsonOut.onclick = function(){
    this.select();
}

function readFile(file){
    return new Promise(resolve=>{
        const reader = new FileReader();
        reader.onload = function(){
            resolve(this.result);
        }
        reader.readAsArrayBuffer(file);
    });
}

function parseNbt(buf){
    return new Promise((resolve, reject)=>{
        nbt.parse(Buffer.from(buf), function(error, data) {
            if (error) return reject(error);
            resolve(data);
        });
    });
}


function srcFromImgData(imgData){
    const canvas = document.createElement('canvas');
    const ctx = canvas.getContext('2d');
    canvas.width = imgData.width;
    canvas.height = imgData.height;
    ctx.putImageData(imgData, 0, 0);
    return canvas.toDataURL();
}

// I fucking hate CSS I swear
function getCanvasBound(){
    const cssBound = mapCanvas.getBoundingClientRect();
    if((cssBound.width / cssBound.height) > (mapCanvas.width / mapCanvas.height)){
        const realWidth = mapCanvas.width / mapCanvas.height * cssBound.height;
        const realX = cssBound.x + (cssBound.width - realWidth) / 2;
        return {
            x: realX,
            y: cssBound.y,
            width: realWidth,
            height: cssBound.height
        }
    }else{
        const realHeight = mapCanvas.height / mapCanvas.width * cssBound.width;
        const realY = cssBound.y + (cssBound.height - realHeight) / 2;
        return {
            x: cssBound.x,
            y: realY,
            width: cssBound.width,
            height: realHeight
        }
    }
}

mapCanvas.ondragover = function(event){
    event.stopPropagation();
    event.preventDefault();
}

// fuck mozilla https://bugzilla.mozilla.org/show_bug.cgi?id=505521
let dragging;
mapCanvas.ondrop = function(event){
    if(!dragging) return;
    console.log(dragging);
    const bound = getCanvasBound();
    const mapX = ((event.pageX - bound.x) / bound.width) * mapCanvas.width;
    const mapY = ((event.pageY - bound.y) / bound.height) * mapCanvas.height;
    if(mapX < 0 || mapX > mapCanvas.width || mapY < 0 || mapY > mapCanvas.height) return;
    const tileX = Math.floor(mapX / 128);
    const tileY = Math.floor(mapY / 128);
    mapCtx.putImageData(dragging.imgData, tileX * 128, tileY * 128);
    mapArrangement[tileY * mapWidth.value + tileX] = {
        x: tileX,
        y: tileY,
        id: dragging.id,
        data: dragging.data,
        rotation: dragging.rotation
    }
}

function addMapToList(data, id){
    const li = document.createElement('li');
    const div = document.createElement('div');
    li.appendChild(div);
    const idLabel = document.createElement('h4');
    idLabel.textContent = id;
    div.appendChild(idLabel);
    const rotationButton = document.createElement('button');
    rotationButton.innerHTML = '&#10227;';
    let rotation = 0;
    const img = new Image();
    let imgData;
    rotationButton.onclick = function(){
        rotation++;
        if(rotation > 3) rotation = 0;
        imgData = renderImage(data, rotation);
        img.src = srcFromImgData(imgData);
    }
    imgData = renderImage(data);
    img.src = srcFromImgData(imgData);
    img.className = 'draghand';
    img.ondragstart = function(){
        dragging = {imgData, id, data, rotation};
    }
    img.ondragend = function(){
        dragging = null
    }
    div.appendChild(img);
    div.appendChild(rotationButton);
    datList.appendChild(li);
}

upload.onchange = async function(){
    for(let file of this.files){
        let data = await readFile(file);
        console.log(data, file.name);
        let nbt = await parseNbt(data).catch(err=>{
            alert(err);
        });
        if(!nbt) continue;
        let rawData = nbt?.value?.data?.value?.colors?.value;
        if(!rawData) continue;
        let colorData = Uint8Array.from(rawData);
        let mapID = file.name.match(/map_([0-9]+)\.dat/)[1];
        console.log(colorData, mapID);
        addMapToList(colorData, mapID);
    }
}

function downloadFile(data, filename){
    const blob = new Blob([data], {type: 'application/octet-stream'});
    const downloadBtn = document.createElement('a');
    downloadBtn.href = URL.createObjectURL(blob);
    downloadBtn.download = filename;
    document.body.appendChild(downloadBtn);
    downloadBtn.click();
    downloadBtn.remove();
}

function scaleTextarea(box){
    const splitText = box.value.split('\n');
    box.rows = splitText.length;
    let cols = 0;
    for(let line of splitText){
        if(line.length > cols) cols = line.length;
    }
    box.cols = cols;
}

exportBtn.onclick = function(){
    jsonOut.style.display = '';
    const downloadedIDs = [];
    jsonOut.value = JSON.stringify({
        name: title.value,
        authors: authors.value.split(','),
        width: mapWidth.value,
        height: mapHeight.value,
        maps: mapArrangement.map(map=>{
            if(!downloadedIDs.includes(map.id)){
                downloadFile(map.data, map.id + '.bin');
                downloadedIDs.push(map.id);
            }
            let piece = {x: map.x, y: map.y, id: map.id}
            if(map.rotation) piece.rotation = map.rotation;
            return piece;
        }),
    }, null ,4);
    scaleTextarea(jsonOut);
}

scrollToTop.onclick = function(){
    options.scrollTo(0, 0);
}

scrollToBottom.onclick = function(){
    settings.scrollIntoView();
}
